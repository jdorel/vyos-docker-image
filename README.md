# Build Vyos image

To build the Vyos image, execute `./build.sh` from this folder.

To start the container, execute `docker-compose up -d`

To connect to the container, execute `./connect.sh`

## connect.sh

Options:
  - `-u <username>`
  - `-c <container>`

## Requirements

To build `vyos-base`, you need to have the following installed:
  - squashfs-tools
  - tar

To build `vyos`, you don't need anything else.

## Build process details

The build process is composed of two steps:
  - build the `vyos-base` docker image using the latest vyos iso. You can do this with the `./build.sh vyos-base`
  - build the `vyos` docker image using the provided Dockerfile. You can do this with `./build.sh vyos`

## Troubleshooting

### Unable to find user vyos

When connecting to vyos, you might get the following error
```
unable to find user vyos: no matching entries in passwd file
```
In this case, just wait a few seconds for the system to finish booting up and try again.

## Acknowledgement

This is heavily inspired from https://github.com/ravens/docker-vyos.
