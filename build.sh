function build_vyos_base_from_iso() {
	cd builds/vyos-base
	./build.sh
	cd -
}

function build_vyos_from_vyos_base() {
	cd builds/vyos
	./build.sh
	cd -
}

function build_vyos () {
  build_vyos_base_from_iso
  build_vyos_from_vyos_base
}

if [[ $# == 0 ]]; then
  build_vyos
elif [[ $1 == vyos-base ]]; then
  build_vyos_base_from_iso
elif [[ $1 == vyos ]]; then
  build_vyos_from_vyos_base
fi
