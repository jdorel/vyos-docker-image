CONTAINER=vyos
USER=vyos

while [[ $# != 0 ]]; do
  if [[ $1 == "-c" ]]; then
    CONTAINER=$2
    shift 2
  elif [[ $1 == "-u" ]]; then
    USER=$2
    shift 2
  fi
done

docker-compose exec -u $USER $CONTAINER /bin/vbash
