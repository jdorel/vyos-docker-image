#! /usr/bin/env bash

DOCKER_IMAGE_NAME="vyos-base"

function download-latest-vyos-iso() {
  mkdir -p downloads
  cd downloads
  wget https://downloads.vyos.io/rolling/current/amd64/vyos-rolling-latest.iso
  cd ..
}

function extract-vyos-filesystem() {
  echo "Extracting filesystem"
  mkdir -p rootfs
  sudo mount -o loop downloads/vyos-rolling-latest.iso rootfs

  mkdir -p unsquashfs
  sudo unsquashfs -f -d unsquashfs rootfs/live/filesystem.squashfs

  sudo umount rootfs
}

function create-docker-image-from-vyos-filesystem() {
  echo "Creating docker image"
  sudo tar -C unsquashfs -c . | docker import - $DOCKER_IMAGE_NAME
}

if [[ $# == 0 ]]; then
  download-latest-vyos-iso
  extract-vyos-filesystem
  create-docker-image-from-vyos-filesystem
elif [[ $1 == "download" ]]; then
  download-latest-vyos-iso
elif [[ $1 == "extract" ]]; then
  extract-vyos-filesystem
elif [[ $1 == "build" ]]; then
  create-docker-image-from-vyos-filesystem
fi
